var colors = {
  light: [13, 13, 13],
  dark: [0, 0, 0],
  medium: [128, 128, 128],
  walkers: [
    [255, 255, 255],
    [128, 128, 128],
    [64, 64, 64],
    [32, 32, 32]
  ]
}

arrays = [create2DArray(16, 16, 0, true), create2DArray(16, 16, 0, true), create2DArray(16, 16, 0, true), create2DArray(16, 16, 0, true)]
walkers = [
  {
    x: 7,
    y: 7
  },
  {
    x: 8,
    y: 7
  },
  {
    x: 7,
    y: 8
  },
  {
    x: 8,
    y: 8
  }
]

var neighbours = []
var frames = 0
var boardSize

function setup() {
  if (windowWidth >= windowHeight) {
    boardSize = windowHeight - 80
  } else {
    boardSize = windowWidth - 80
  }
  createCanvas(windowWidth, windowHeight)
}

function draw() {
  createCanvas(windowWidth, windowHeight)
  background(colors.light)
  rectMode(CENTER)
  colorMode(RGB, 255, 255, 255, 1)
  frameRate(60)
  // background rectangle
  fill(colors.dark)
  noStroke()
  rect(windowWidth * 0.5, windowHeight * 0.5, boardSize, boardSize)
  // drawing random walker
  for (var i = 0; i < arrays.length; i++) {
    for (var j = 0; j < arrays[i].length; j++) {
      for (var k = 0; k < arrays[i][j].length; k++) {
        fill(colors.walkers[i][0], colors.walkers[i][1], colors.walkers[i][2], 0.666)
        noStroke()
        rect(windowWidth * 0.5 + (j - 7.5) * (42 / 768) * boardSize, windowHeight * 0.5 + (k - 7.5) * (42 / 768) * boardSize, Math.floor(arrays[i][j][k] * 3.00 + 0.5) * (42 / 768) * boardSize / 3.00, Math.floor(arrays[i][j][k] * 3.00 + 0.5) * (42 / 768) * boardSize / 3.00)
      }
    }
  }
  // grid points
  for (var i = 0; i < 17; i++) {
    for (var j = 0; j < 17; j++) {
      fill(colors.medium)
      noStroke()
      ellipse(windowWidth * 0.5 + (i - 8) * (42 / 768) * boardSize, windowHeight * 0.5 + (j - 8) * (42 / 768) * boardSize, 3, 3)
    }
  }

  // updating random walker
  frames += deltaTime * 0.025
  if (frames > 1) {
    frames = 0

    for (var i = 0; i < arrays.length; i++) {
      arrays[i][walkers[i].x][walkers[i].y] = 1
      var neighbours = getNeighbours(walkers[i].x, walkers[i].y, arrays[i])
      var rand = Math.floor(Math.random() * neighbours.length)
      if (neighbours[rand] !== undefined) {
        walkers[i].x = neighbours[rand][0]
        walkers[i].y = neighbours[rand][1]
      }
      neighbours = []
      for (var j = 0; j < arrays[i].length; j++) {
        for (var k = 0; k < arrays[i][j].length; k++) {
          if (arrays[i][j][k] < 0.9) {
            if (dist(walkers[i].x, walkers[i].y, j, k) < 8 && arrays[i][j][k] !== 1) {
              arrays[i][j][k] = 1.2 - dist(walkers[i].x, walkers[i].y, j, k) * 0.2
            }
          }
          if (arrays[i][j][k] > 0) {
            arrays[i][j][k] -= 0.002 + 0.018 * (mouseX / windowWidth)
          } else {
            arrays[i][j][k] = 0
          }
        }
      }
    }
  }
}

function windowResized() {
  if (windowWidth >= windowHeight) {
    boardSize = windowHeight - 80
  } else {
    boardSize = windowWidth - 80
  }
  createCanvas(windowWidth, windowHeight)
}

function create2DArray(numRows, numCols, init, bool) {
  var array = [];
  for (var i = 0; i < numRows; i++) {
    var columns = []
    for (var j = 0; j < numCols; j++) {
      if (bool === true) {
        columns[j] = init
      } else {
        columns[j] = j * numRows + i
      }
    }
    array[i] = columns
  }
  return array
}

function getNeighbours(x, y, array) {
  var neighbour = []
  if (array[(x + 1) % array.length][y] < 1) {
    neighbour.push([(x + 1) % array.length, y])
  }
  if (array[(array.length + (x - 1)) % array.length][y] < 1) {
    neighbour.push([(array.length + (x - 1)) % array.length, y])
  }
  if (array[x][(y + 1) % array.length] < 1) {
    neighbour.push([x, (y + 1) % array.length])
  }
  if (array[x][(array.length + (y - 1)) % array.length] < 1) {
    neighbour.push([x, (array.length + (y - 1)) % array.length])
  }
  return neighbour
}
